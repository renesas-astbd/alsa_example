
ifeq ("$(MSVC)", "")
MSVC=0
endif

ifeq ("$(MSVC)", "0")
REFORMAT=cat
else
ANSI=0
REFORMAT=perl -p -e 's/\//\\/g' | perl -p -e 's/^(.*):(\d+):(\d+): (.*): (.*)/\1(\2,\3): \4 : \5/'
endif

# ANSI Colors
ifeq ("$(ANSI)", "")
ANSI=1
endif

ifeq ("$(ANSI)", "0")
NORMAL=
BRED=
BGREEN=
BYELLOW=
BBLUE=
BMAGENTA=
BCYAN=
BWHITE=
else
NORMAL=\33[0m
BRED=\33[31;1m
BGREEN=\33[32;1m
BYELLOW=\33[33;1m
BBLUE=\33[34;1m
BMAGENTA=\33[35;1m
BCYAN=\33[36;1m
BWHITE=\33[37;1m
endif

PROJECT := $(notdir $(CURDIR))
UNAME_S := $(shell uname -s)

ifeq ("$(OS)", "")
OS := $(shell uname -s)
endif

ifeq ("$(PROCESSOR_ARCHITECTURE)", "")
PROCESSOR_ARCHITECTURE := $(shell uname -p)
endif


# These directories will be modified by the DEBUG/RELEASE and OPT {x86_64,ARMv8}
OUT=out
OBJ=obj

ifeq ("$(OPT)","x86_64")
$(info x86_64 optimized release build)
FLAGS+=$(x86_64FLAGS)
OPTFLAGS=$(x86_64FLAGS)
OUT=out/x86_64
OBJ=out/x86_64
endif
ifeq ("$(OPT)","ARMv8")
$(info ARMv8 optimized release build)
FLAGS+=$(ARMv8FLAGS)
OPTFLAGS=$(ARMv8FLAGS)
CC=/opt/poky/2.0.1/sysroots/x86_64-pokysdk-linux/usr/bin/aarch64-poky-linux/aarch64-poky-linux-gcc
FLAGS=--sysroot=/opt/poky/2.0.1/sysroots/aarch64-poky-linux
LDFLAGS=--sysroot=/opt/poky/2.0.1/sysroots/aarch64-poky-linux
OUT=out/armv8
OBJ=out/armv8
endif

# Choose between debug or optimized build flags
ifeq ("$(DEBUG)","")
DEBUG=0
endif
ifneq ("$(DEBUG)","0")
$(info Debug build)
FLAGS+=$(DEBUGFLAGS)
endif


CSRCS=$(wildcard src/*.c)
CPPSRCS=$(wildcard src/*.cpp)
SRCS=$(sort $(filter-out src/test-%,$(CSRCS) $(CPPSRCS)))
TSRCS=$(filter src/test-%,$(CSRCS) $(CPPSRCS))
OBJS=$(sort $(filter-out $(OBJ)/test-%,$(subst src,$(OBJ),$(CSRCS:.c=.o) $(CPPSRCS:.cpp=.o))))

TPROGS=$(subst src,$(OUT),$(TSRCS:.c=))
TOBJS=$(subst src,$(OBJ),$(TSRCS:.c=.o))

LIBS+=asound

FLAGS+=-Wall -pipe -Werror=return-type -fPIC
CFLAGS=-std=gnu89 $(FLAGS)
CXXFLAGS=-std=c++11 $(FLAGS)
x86_64FLAGS=-Ofast -march=native
ARMv8FLAGS=-Ofast -mstrict-align -ffast-math -mfix-cortex-a53-835769 -mfix-cortex-a53-843419 -march=armv8-a+crypto -mtune=cortex-a57
DEBUGFLAGS=-g -Og -fno-omit-frame-pointer
CDEFS=$(foreach DEF,$(DEFS),-D $(DEF))
CPPDEFS=$(CDEFS)

ARFALGS+=
LDFLAGS+=

NODEPS:=clean info
DEPFILES=$(wildcard $(OBJ)/*.d)

all: $(TPROGS)
all:
	@printf "\n"


LIBS+=c

$(OUT)/%: $(OBJ)/%.o
	@mkdir -p $(OUT)
	@printf "$(BGREEN)%s$(NORMAL) <- %s\n" "$@" "$(filter %.so,$^) $(foreach LIB,$(LIBS),lib$(LIB).so)"
	@$(CC) $(LDFLAGS) -o $@ $(filter-out %.so,$^) $(foreach LIB,$(LIBS),-l$(LIB))

$(OBJ)/%.o: src/%.c
	@mkdir -p $(OBJ)
	@printf "%s <- %s\n" "$@" "$<"
	@$(CC) -c -MMD $(CDEFS) $(CFLAGS) -o $@ $< 2>&1 | $(REFORMAT)

$(OBJ)/%.o: src/%.cpp
	@mkdir -p $(OBJ)
	@printf "%s <- %s\n" "$@" "$<"
	@$(CC) -c -MMD $(CPPDEFS) $(CXXFLAGS) -o $@ $< 2>&1 | $(REFORMAT)


$(OUT)/%.a:
	@mkdir -p $(OUT)
	@printf "$(BBLUE)%s$(NORMAL) <- %s\n" "$@" "$^"
	@$(AR) -cr $@ $^

$(OUT)/lib%.so:
	@mkdir -p $(OUT)
	@printf "$(BBLUE)%s$(NORMAL) <- %s\n" "$@" "$^"
	@$(CC) -shared $(LDFLAGS) -o $@ $^

clean:
	@rm -rf $(OUT)/* $(OBJ)/*

cleanall:
	@rm -rf out obj

info:
	@printf "Project   : %s\n" "$(PROJECT)"
	@printf "Library   : %s\n" "$(LIBRARY)"
	@printf "System    : %s %s %s\n" "$(OS)" "$(PROCESSOR_ARCHITECTURE)" #"$(UNAME_S)"
	@printf "C Flags   : %s\n" "$(CFLAGS)"
	@printf "C++ Flags : %s\n" "$(CXXFLAGS)"
	@printf "Dbg Flags : %s\n" "$(DEBUGFLAGS)"
	@printf "Opt Flags : %s\n" "$(OPTFLAGS)"
	@printf "Source    : %s\n" "out/$(LIBRARY).so"
	@printf "            %s\n" $(SRCS)
	@printf "Test      : %s\n" ""
	@printf "            %s\n" $(TSRCS)
	@printf "Object    : %s\n" ""
	@printf "            %s\n" $(OBJS)
	@printf "\n"

deploy: $(OUT)/test-alsa
	scp $(OUT)/test-alsa rcar@salvator:

deploydata:
	scp *.wav rcar@salvator:

remote-test: deploy
	ssh rcar@salvator './test-alsa play.wav'

local-test: $(OUT)/test-waviq
	$(OUT)/test-alsa play.wav

#Don't include dependencies when we're cleaning et. al.
ifeq (0, $(words $(findstring $(MAKECMDGOALS), $(NODEPS))))
-include $(DEPFILES)
endif

.PHONY: all info clean deploy

# Some rules infer which object files to build - disable make's auto-remove them for these cases
.PRECIOUS: $(OBJS) $(TOBJS)

vpath %.c src
vpath %.cpp src
