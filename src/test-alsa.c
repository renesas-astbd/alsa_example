/*******************************************************************************/
/* Name:  test-alsa.c                                                          */
/*                                                                             */
/* Description: Program to test the Linux ALSA interfaces by playing wave      */
/* files (.WAV) using the ALSA API's.  This program must be linked with        */
/* the ALSA library "asoundlib".                                               */
/*                                                                             */
/* This file was formatted with the "indent" package as follows:               */
/* $indent -l120 -i4 -bli0 -nut <source_file.c>                                */
/*******************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <termios.h>
#include <signal.h>

#include <alsa/asoundlib.h>
#include <alsa/mixer.h>

/*******************************************************************************/
/*  MACROS                                                                     */
/*******************************************************************************/

/*
** BUFFER_SIZE - size of audio buffer in bytes.
*/
#define BUFFER_SIZE (1024*4)

#define  MIN_VOLUME   (1)
#define  MAX_VOLUME   (50)

/*
** VOLUME_ELEM - name of ALSA volume control element.
*/
#define  VOLUME_ELEM   "DVC Out"

/*
** AUDIO_DEV_NAME - audio device name 
** AUDIO_ENABLE_ELEM - element that enables audio output
*/
#define AUDIO_DEV_NAME "hw:0,0"
#define AUDIO_ENABLE_ELEM  "DVC Out Mute"

/*******************************************************************************/
/*  FUNCTION PROTOTYPES                                                        */
/*******************************************************************************/
static int set_hwparams (snd_pcm_t * handle, snd_pcm_access_t access);
static int set_swparams (snd_pcm_t * handle);
static void PrintHwParams (snd_pcm_t * handle);
static void CheckKeyboardInput (void);
static void ConfigKeyboard (void);
static void RestoreKeyboard (void);
static void SetVolumePercent (long volume_percent);
static void ErrorExit (int error_code);

/*******************************************************************************/
/*  TYPE DEFINITIONS                                                           */
/*******************************************************************************/
/*
** WAVE file header
*/
typedef struct
{
    char chunk_id[4];           /* must contain the string "RIFF" (big endian) */
    unsigned int chunk_size;    /* size of remainder of the file in bytes (little endian) */
    char format[4];             /* must contain the string "WAVE" (big endian) */
    char subchunk1_id[4];       /* must contain the string "fmt" followed by null char (big endian) */
    unsigned int subchunk1_size;        /* size of subchunk1 in bytes...must be 16 for a wave file (little endian) */
    unsigned short audio_format;        /* must be 1 (linear quantizatin) for a wave file (little endian) */
    unsigned short num_channels;        /* number of channels (1=mon, 2=stereo) (little endian) */
    unsigned int sample_rate;   /* number of samples per second  (little endian) */
    unsigned int byte_rate;     /* bytes per second (sample_rate * num_channels * bits_per_sample/8) (little endian) */
    unsigned short block_align; /* num_channels * bits_per_second/8   (little endian) */
    unsigned short bits_per_sample;     /* bits per sample, (8=8bits, 16=16 bits) (little endian) */
    char data_chunk_header[4];  /* must contain the string "data" for a wave file (big endian) */
    unsigned int data_size;     /* number of data bytes (little endian) */
} t_waveHeader;


/*******************************************************************************/
/*  GLOBALS                                                                    */
/*******************************************************************************/

static t_waveHeader file_header;        /* stores header read from wav file */
static int fd;                  /* file desciptor of wav file */
static unsigned int buffer_time = 500000;  /* ring buffer length in us */
//static unsigned int buffer_time = 300000;       /* ring buffer length in us */
static unsigned int period_time = 100000;       /* period time in us */
static int period_event = 0;    /* don't produce poll event after each period */
static unsigned int frame_size_bytes;   /* frame size in bytes */
static unsigned int number_channels;    /* number of audio channels */
static unsigned int bits_per_sample;    /* self explanatory */
static unsigned int sample_rate;        /* self explanatory */
static snd_pcm_access_t pcm_format;     /* format of wav file data */
static snd_pcm_sframes_t buffer_size;   /* buffer size in bytes */
static snd_pcm_sframes_t period_size;   /* period size in frames */
static char *device = AUDIO_DEV_NAME;   /* audio device */
static int file_size_bytes = 0; /* self explanatory */
static int file_bytes_read = 0; /* self explanatory */
static unsigned int playback_volume = 8;        /* self explanatory */

/*******************************************************************************/
static void
PrintHeader (void)
{
    printf ("File Header (%ld bytes) \n", sizeof (file_header));
    printf ("ChunkID       = %c%c%c%c \n", file_header.chunk_id[0], file_header.chunk_id[1], file_header.chunk_id[2],
            file_header.chunk_id[3]);
    printf ("ChunkSize     = %u \n", file_header.chunk_size);
    printf ("Format        = %c%c%c%c \n", file_header.format[0], file_header.format[1], file_header.format[2],
            file_header.format[3]);
    printf ("SubChunk1ID   = %c%c%c%c \n", file_header.subchunk1_id[0], file_header.subchunk1_id[1],
            file_header.subchunk1_id[2], file_header.subchunk1_id[3]);
    printf ("SubChunk1Size = %u \n", file_header.subchunk1_size);
    printf ("AudioFormat   = %u (%s) \n", file_header.audio_format, file_header.audio_format == 1 ? "PCM" : "NON-PCM");
    printf ("NumChannels   = %u \n", file_header.num_channels);
    printf ("SampleRate    = %u \n", file_header.sample_rate);
    printf ("ByteRate      = %u \n", file_header.byte_rate);
    printf ("BlockAlign    = %u \n", file_header.block_align);
    printf ("BitsPerSample = %u \n", file_header.bits_per_sample);
    printf ("SubChunk2ID   = %c%c%c%c \n", file_header.data_chunk_header[0], file_header.data_chunk_header[1],
            file_header.data_chunk_header[2], file_header.data_chunk_header[3]);
    printf ("SubChunk2Size = %u \n", file_header.data_size);
}

/*******************************************************************************/
static void
OpenAudioFile (char *file_name)
{
    ssize_t bytes_read;
    struct stat file_stat;

    /* 
     ** Open file, read in the header, and get file size.
     */
    fd = open (file_name, O_RDONLY);
    if (fd == -1)
    {
        printf ("OpenAudioFile: Error opening file \"%s\", errno=%d\n", file_name, errno);
        ErrorExit (1);
    }

    bytes_read = read (fd, &file_header, sizeof (file_header));
    if (bytes_read == -1)
    {
        printf ("OpenAudioFile: read returned -1, errno=%d \n", errno);
        ErrorExit (1);
    }

    PrintHeader ();

    if (fstat (fd, &file_stat) < 0)
    {
        printf ("OpenAudioFile: fstat returned < 0, errno=%d \n", errno);
        ErrorExit (1);
    }
    file_size_bytes = file_stat.st_size;

    /*
     ** Verify wav format.
     */
    if (strncmp (&file_header.chunk_id[0], "RIFF", 4) != 0)
    {
        printf ("OpenAudioFile: Header field \"ChunkID\" is invalid \n");
        ErrorExit (1);
    }
    if (strncmp (&file_header.format[0], "WAVE", 4) != 0)
    {
        printf ("OpenAudioFile: Header field \"Format\" is invalid \n");
        ErrorExit (1);
    }
    if (strncmp (&file_header.subchunk1_id[0], "fmt ", 4) != 0)
    {
        printf ("OpenAudioFile: Header field \"SubChunk1ID\" is invalid \n");
        ErrorExit (1);
    }
    if (strncmp (&file_header.data_chunk_header[0], "data", 4) != 0)
    {
        printf ("OpenAudioFile: Header field \"SubChunk2ID\" is invalid \n");
        ErrorExit (1);
    }

    frame_size_bytes = file_header.byte_rate / file_header.sample_rate;
    bits_per_sample = file_header.bits_per_sample;
    number_channels = file_header.num_channels;
    sample_rate = file_header.sample_rate;

    switch (bits_per_sample)
    {
    case 8:
        pcm_format = SND_PCM_FORMAT_U8; /* mono is always 8-bit unsigned for PCM files */
        printf ("OpenAudioFile: pcm_format is SND_PCM_FORMAT_U8\n");
        break;
    case 16:
        pcm_format = SND_PCM_FORMAT_S16;        /* stereo is always 16-bit signed for PCM files */
        printf ("OpenAudioFile: pcm_format is SND_PCM_FORMAT_S16\n");
        break;
    default:
        printf ("OpenAudioFile: \"bits_per_sample\" not supported (value=%u)\n", bits_per_sample);
        ErrorExit (1);
    }
    printf ("OpenAudioFile: frame_size_bytes=%u, bits_per_sample=%u, number_channels=%u, sample_rate=%u \n",
            frame_size_bytes, bits_per_sample, number_channels, sample_rate);
}


/*******************************************************************************/
static unsigned int
ReadAudioFile (unsigned char *buffer, unsigned int bytes_to_read)
{
    ssize_t bytes_read;

    bytes_read = read (fd, buffer, bytes_to_read);
    if (bytes_read == -1)
    {
        printf ("ReadAudioFile: read returned -1, errno=%d \n", errno);
        ErrorExit (1);
    }

    return ((unsigned int) bytes_read);
}

/*******************************************************************************/
static void
StreamAudioSimple (void)
{
    int err;
    snd_pcm_t *handle;
    snd_pcm_sframes_t frames;
    unsigned long frames_to_write = 1;
    unsigned long frames_written;
    unsigned long write_size;
    unsigned char buffer[BUFFER_SIZE];
    unsigned int frames_streamed = 0;
    unsigned int frames_attempted = 0;
    unsigned int bytes_read;

    if ((err = snd_pcm_open (&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0)
    {
        printf ("StreamAudioSimple: Playback open error: %s\n", snd_strerror (err));
        ErrorExit (1);
    }

    set_hwparams (handle, SND_PCM_ACCESS_RW_INTERLEAVED);
    PrintHwParams (handle);
    set_swparams (handle);
    printf ("\nStreamAudioSimple: begin streaming...type \"V\" for VOLUME+ or \"v\" for VOLUME-\n\n\n");

    /* Loop while more bytes left in the file */
    while (frames_to_write > 0)
    {
        /* Read next chunk of audio data and stream it */
        bytes_read = ReadAudioFile (buffer, sizeof (buffer));
        frames_to_write = bytes_read / frame_size_bytes;
        file_bytes_read += bytes_read;

        if (frames_to_write > 0)
        {
            frames_written = 0;
            /* Continue continue streaming while more bytes to write.  The ALSA interface may not accept all bytes in one write */
            while (frames_written < frames_to_write)
            {
                write_size = frames_to_write - frames_written;
                frames_attempted += write_size;
                frames = snd_pcm_writei (handle, &buffer[frames_written], write_size);
                frames_written += frames;
                frames_streamed += frames;
                printf ("\033[1AStreamAudioSimple: frames attempted/success  %08u/%08u   percent complete: %5.1f\n",
                        frames_attempted, frames_streamed,
                        (float) ((file_bytes_read / (float) file_size_bytes) * 100.0));
                if (frames < 0)
                {
                    printf ("StreamAudioSimple: call snd_pcm_recover!!! \n");
                    frames = snd_pcm_recover (handle, frames, 0);
                }
                if (frames < 0)
                {
                    printf ("StreamAudioSimple: snd_pcm_writei failed: %s\n", snd_strerror (frames));
                    ErrorExit (1);
                }
                if (frames > 0 && frames < write_size)
                    printf ("StreamAudioSimple: Short write (write_size=%lu, frames=%lu)\n", write_size, frames);
            }
        }
        CheckKeyboardInput ();
    }
    printf ("StreamAudioSimple: complete \n\n");
    snd_pcm_close (handle);
}

/*******************************************************************************/
static void
StreamAudioRing (void)
{
    snd_pcm_t *handle;
    snd_pcm_uframes_t frames;
    const snd_pcm_channel_area_t *my_areas;
    snd_pcm_uframes_t offset;
    unsigned long frames_written = 0;
    unsigned int frames_streamed = 0;
    unsigned int frames_to_write = 0;
    unsigned int frames_attempted = 0;
    unsigned int times_no_space_avail = 0;
    unsigned int bytes_read = 1;
    unsigned int first = 1;
    unsigned char *fill_addr;
    int ret;

    if ((ret = snd_pcm_open (&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0)
    {
        printf ("StreamAudioRing: Playback open error: %s\n", snd_strerror (ret));
        ErrorExit (1);
    }

    set_hwparams (handle, SND_PCM_ACCESS_MMAP_INTERLEAVED);
    PrintHwParams (handle);
    set_swparams (handle);

    printf ("\nStreamAudioRing: begin streaming...type \"V\" for VOLUME+ or \"v\" for VOLUME-\n\n\n");

    /* Loop while more bytes left in file */
    while (bytes_read > 0)
    {
        ret = snd_pcm_avail_update (handle);
        if (ret > 0)
        {
            /* Space available in the ring buffer, so read next chunk of audio data and stream it */
            if ((snd_pcm_state (handle) == SND_PCM_STATE_XRUN) ||
                (snd_pcm_state (handle) == SND_PCM_STATE_PAUSED) ||
                (snd_pcm_state (handle) == SND_PCM_STATE_SUSPENDED) ||
                (snd_pcm_state (handle) == SND_PCM_STATE_DRAINING) ||
                (snd_pcm_state (handle) == SND_PCM_STATE_DISCONNECTED))
            {
                /* These error conditions are not handled in this simple example */
                printf ("StreamAudioRing: snd_pcm_state return is %d\n", snd_pcm_state (handle));
                ErrorExit (1);
            }

            frames = period_size;
            ret = snd_pcm_mmap_begin (handle, &my_areas, &offset, &frames);
            if (ret != 0)
            {
                printf ("StreamAudioRing: snd_pcm_mmap_begin error: %s\n", snd_strerror (ret));
                ErrorExit (1);
            }

            if ((frame_size_bytes * 8) != my_areas->step)
            {
                printf ("StreamAudioRing: Error frame_size_bytes*8=%d  my_areas->step=%d \n",
                        frame_size_bytes * 8, my_areas->step);
                ErrorExit (1);
            }

            fill_addr = (unsigned char *) (my_areas->addr + (my_areas->first / 8) + (offset * frame_size_bytes));

            bytes_read = ReadAudioFile (fill_addr, frames * frame_size_bytes);

            file_bytes_read += bytes_read;
            frames_to_write = bytes_read / frame_size_bytes;
            frames_attempted += frames_to_write;
#if 0
            printf ("addr=%X  first=%d  step=%d  offset=%u  frames_to_write=%u  fill_addr=%X\n\n\n",
                    my_areas->addr, my_areas->first, my_areas->step, offset, frames_to_write, fill_addr);
#endif
            frames_written = snd_pcm_mmap_commit (handle, offset, frames_to_write);
            if (frames_written != frames_to_write)
            {
                printf ("StreamAudioRing: snd_pcm_mmap_commit returned %lu, expected %u \n", frames_written,
                        (unsigned int) frames_to_write);
                ErrorExit (1);
            }
            frames_streamed += frames_written;
            printf ("\033[1AStreamAudioRing: frames attempted/success  %08u/%08u   percent complete: %5.1f\n",
                    frames_attempted, frames_streamed, (float) ((file_bytes_read / (float) file_size_bytes) * 100.0));
            if (first == 1)
            {
                first = 0;
                if ((ret = snd_pcm_start (handle)) < 0)
                {
                    printf ("StreamAudioRing: snd_pcm_start error: %s\n", snd_strerror (ret));
                    printf ("StreamAudioRing: snd_pcm_state return is %d\n", snd_pcm_state (handle));
                    ErrorExit (1);
                }
            }
        }
        else if (ret == 0)
        {
            ++times_no_space_avail;
        }
        else
        {
            printf ("StreamAudioRing: snd_pcm_avail_update returned %d\n", ret);
            ErrorExit (1);
        }
        CheckKeyboardInput ();
    }
    printf ("StreamAudioRing: complete (times_no_space_avail=%u) \n\n", times_no_space_avail);
    snd_pcm_close (handle);
}

/*******************************************************************************/
static void
PrintUsage (void)
{
    printf ("Usage: test-alsa <file-name> \n");
    printf ("       <file-name> = name of wave (.WAV) file to play \n");
    exit (0);
}

/*******************************************************************************/
int
main (int argc, char *argv[])
{
    if (argc < 2)
    {
        PrintUsage ();
    }

    ConfigKeyboard ();
    SetVolumePercent (playback_volume);

    OpenAudioFile (argv[1]);

    if (argc == 2)
    {
        StreamAudioRing ();
    }
    else
    {
        StreamAudioSimple ();
    }
    RestoreKeyboard ();

    return (0);
}

/*******************************************************************************/
static int
set_hwparams (snd_pcm_t * handle, snd_pcm_access_t access)
{

    snd_pcm_hw_params_t *hwparams;
    unsigned int resample = 1;  /* enable resampling */
    unsigned int rrate = 1; 
    snd_pcm_uframes_t size;
    int err, dir;

    /* Allocate buffer for hw params */
    snd_pcm_hw_params_alloca (&hwparams);

    /* choose all parameters */
    err = snd_pcm_hw_params_any (handle, hwparams);
    if (err < 0)
    {
        printf ("Broken configuration for playback: no configurations available: %s\n", snd_strerror (err));
        ErrorExit (1);
    }

    /* set the interleaved read/write format */
    //printf ("set_hwparams: set access %d \n", access);
    err = snd_pcm_hw_params_set_access (handle, hwparams, access);
    if (err < 0)
    {
        printf ("Access type not available for playback: %s\n", snd_strerror (err));
        ErrorExit (1);
    }

    /* set the sample format */
    //printf ("set_hwparams: set format %d \n", pcm_format);
    err = snd_pcm_hw_params_set_format (handle, hwparams, pcm_format);
    if (err < 0)
    {
        printf ("Sample format not available for playback: %s\n", snd_strerror (err));
        ErrorExit (1);
    }
    /* set the count of channels */
    //printf ("set_hwparams: set channels %d \n", number_channels);
    err = snd_pcm_hw_params_set_channels (handle, hwparams, number_channels);
    if (err < 0)
    {
        printf ("Channels count (%i) not available for playbacks: %s\n", number_channels, snd_strerror (err));
        ErrorExit (1);
    }
    
    /* set hardware resampling */
    err = snd_pcm_hw_params_set_rate_resample (handle, hwparams, resample);
    if (err < 0)
    {
        printf ("Resampling setup failed for playback: %s\n", snd_strerror (err));
        ErrorExit (1);
    }
    
    /* set the target sample rate */
    rrate = sample_rate;
    printf ("set_hwparams: attempt to set rate to %d \n", rrate);
    err = snd_pcm_hw_params_set_rate_near (handle, hwparams, &rrate, 0);
    if (err < 0)
    {
        printf ("Rate %iHz not available for playback: %s\n", rrate, snd_strerror (err));
        ErrorExit (1);
    }
    if (rrate != sample_rate)
    {
        printf ("Rate doesn't match!!!!! (requested %iHz, got %iHz)\n", sample_rate, rrate);
        //  ErrorExit (1);
    }

    /* set the buffer time */
    err = snd_pcm_hw_params_set_buffer_time_near (handle, hwparams, &buffer_time, &dir);
    if (err < 0)
    {
        printf ("Unable to set buffer time %i for playback: %s\n", buffer_time, snd_strerror (err));
        ErrorExit (1);
    }
    err = snd_pcm_hw_params_get_buffer_size (hwparams, &size);
    if (err < 0)
    {
        printf ("Unable to get buffer size for playback: %s\n", snd_strerror (err));
        ErrorExit (1);
    }
    buffer_size = size;
    /* set the period time */
    err = snd_pcm_hw_params_set_period_time_near (handle, hwparams, &period_time, &dir);
    if (err < 0)
    {
        printf ("Unable to set period time %i for playback: %s\n", period_time, snd_strerror (err));
        ErrorExit (1);
    }
    err = snd_pcm_hw_params_get_period_size (hwparams, &size, &dir);
    if (err < 0)
    {
        printf ("Unable to get period size for playback: %s\n", snd_strerror (err));
        ErrorExit (1);
    }
    period_size = size;
    
    /* write the parameters to device */
    err = snd_pcm_hw_params (handle, hwparams);
    if (err < 0)
    {
        printf ("Unable to set hw params for playback: %s\n", snd_strerror (err));
        ErrorExit (1);
    }
    
    printf ("set_hwparams: buffer_size=%d, period_size=%d, rate=%u, buffer_time=%u, period_time=%u\n",
            (int) buffer_size, (int) period_size, rrate, buffer_time, period_time);
    
    return 0;
}

/*******************************************************************************/
static int
set_swparams (snd_pcm_t * handle)
{
    int err;
    snd_pcm_sw_params_t *swparams;
    snd_pcm_uframes_t avail_min;
    snd_pcm_uframes_t start_threshold;


    snd_pcm_sw_params_alloca (&swparams);

    /* get the current swparams */
    err = snd_pcm_sw_params_current (handle, swparams);
    if (err < 0)
    {
        printf ("Unable to determine current swparams for playback: %s\n", snd_strerror (err));
        ErrorExit (1);
    }
    /* start the transfer when the buffer is almost full: */
    /* (buffer_size / avail_min) * avail_min */
    err = snd_pcm_sw_params_set_start_threshold (handle, swparams, (buffer_size / period_size) * period_size);
    if (err < 0)
    {
        printf ("Unable to set start threshold mode for playback: %s\n", snd_strerror (err));
        ErrorExit (1);
    }
    /* allow the transfer when at least period_size samples can be processed */
    /* or disable this mechanism when period event is enabled (aka interrupt like style processing) */
    err = snd_pcm_sw_params_set_avail_min (handle, swparams, period_event ? buffer_size : period_size);
    if (err < 0)
    {
        printf ("Unable to set avail min for playback: %s\n", snd_strerror (err));
        ErrorExit (1);
    }
    /* enable period events when requested */
    if (period_event)
    {
        err = snd_pcm_sw_params_set_period_event (handle, swparams, 1);
        if (err < 0)
        {
            printf ("Unable to set period event: %s\n", snd_strerror (err));
            ErrorExit (1);
        }
    }

    err = snd_pcm_sw_params_get_avail_min (swparams, &avail_min);
    if (err < 0)
    {
        printf ("Unable to get avail min: %s\n", snd_strerror (err));
        ErrorExit (1);
    }

    err = snd_pcm_sw_params_get_start_threshold (swparams, &start_threshold);
    if (err < 0)
    {
        printf ("Unable to get avail min: %s\n", snd_strerror (err));
        ErrorExit (1);
    }

    printf ("set_swparams: avail_min=%u, start_threshold=%u \n", (unsigned) avail_min, (unsigned) start_threshold);

    /* write the parameters to the playback device */
    err = snd_pcm_sw_params (handle, swparams);
    if (err < 0)
    {
        printf ("Unable to set sw params for playback: %s\n", snd_strerror (err));
        ErrorExit (1);
    }
    return 0;
}

/*******************************************************************************/
void
PrintHwParams (snd_pcm_t * handle)
{
#if 0
    snd_pcm_hw_params_t *params;
    unsigned int val, val2;
    int dir;
    snd_pcm_uframes_t frames;

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca (&params);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any (handle, params);


    /* Display information about the PCM interface */

    printf ("PCM handle name = '%s'\n", snd_pcm_name (handle));

    printf ("PCM state = %s\n", snd_pcm_state_name (snd_pcm_state (handle)));

    snd_pcm_hw_params_get_access (params, (snd_pcm_access_t *) & val);
    printf ("access type = %s\n", snd_pcm_access_name ((snd_pcm_access_t) val));

    snd_pcm_hw_params_get_format (params, (snd_pcm_format_t *) & val);
    printf ("format = '%s' (%s)\n",
            snd_pcm_format_name ((snd_pcm_format_t) val), snd_pcm_format_description ((snd_pcm_format_t) val));

    snd_pcm_hw_params_get_subformat (params, (snd_pcm_subformat_t *) & val);
    printf ("subformat = '%s' (%s)\n",
            snd_pcm_subformat_name ((snd_pcm_subformat_t) val),
            snd_pcm_subformat_description ((snd_pcm_subformat_t) val));

    snd_pcm_hw_params_get_channels (params, &val);
    printf ("channels = %d\n", val);

    snd_pcm_hw_params_get_rate (params, &val, &dir);
    printf ("rate = %d bps\n", val);

    snd_pcm_hw_params_get_period_time (params, &val, &dir);
    printf ("period time = %d us\n", val);

    snd_pcm_hw_params_get_period_size (params, &frames, &dir);
    printf ("period size = %d frames\n", (int) frames);

    snd_pcm_hw_params_get_buffer_time (params, &val, &dir);
    printf ("buffer time = %d us\n", val);

    snd_pcm_hw_params_get_buffer_size (params, (snd_pcm_uframes_t *) & val);
    printf ("buffer size = %d frames\n", val);

    snd_pcm_hw_params_get_periods (params, &val, &dir);
    printf ("periods per buffer = %d frames\n", val);

    snd_pcm_hw_params_get_rate_numden (params, &val, &val2);
    printf ("exact rate = %d/%d bps\n", val, val2);

    val = snd_pcm_hw_params_get_sbits (params);
    printf ("significant bits = %d\n", val);

    val = snd_pcm_hw_params_is_batch (params);
    printf ("is batch = %d\n", val);

    val = snd_pcm_hw_params_is_block_transfer (params);
    printf ("is block transfer = %d\n", val);

    val = snd_pcm_hw_params_is_double (params);
    printf ("is double = %d\n", val);

    val = snd_pcm_hw_params_is_half_duplex (params);
    printf ("is half duplex = %d\n", val);

    val = snd_pcm_hw_params_is_joint_duplex (params);
    printf ("is joint duplex = %d\n", val);

    val = snd_pcm_hw_params_can_overrange (params);
    printf ("can overrange = %d\n", val);

    val = snd_pcm_hw_params_can_mmap_sample_resolution (params);
    printf ("can mmap = %d\n", val);

    val = snd_pcm_hw_params_can_pause (params);
    printf ("can pause = %d\n", val);

    val = snd_pcm_hw_params_can_resume (params);
    printf ("can resume = %d\n", val);

    val = snd_pcm_hw_params_can_sync_start (params);
    printf ("can sync start = %d\n", val);
#endif
}

/*******************************************************************************/
static const char *card = "hw:0";
static void
SetVolumePercent (long volume_percent)
{

    long min, max;
    long read_volume = 9999;
    snd_mixer_t *handle;
    snd_mixer_selem_id_t *sid;
    snd_mixer_elem_t *elem;
    int ret;

    ret = snd_mixer_open (&handle, 0);
    if (ret != 0)
    {
        printf ("snd_mixer_open returned error: %s\n", snd_strerror (ret));
        ErrorExit (1);
    }

    ret = snd_mixer_attach (handle, card);
    if (ret != 0)
    {
        printf ("snd_mixer_attach returned error: %s\n", snd_strerror (ret));
        ErrorExit (1);
    }

    ret = snd_mixer_selem_register (handle, NULL, NULL);
    if (ret != 0)
    {
        printf ("snd_mixer_register returned error: %s\n", snd_strerror (ret));
        ErrorExit (1);
    }

    ret = snd_mixer_load (handle);
    if (ret != 0)
    {
        printf ("snd_mixer_load returned error: %s\n", snd_strerror (ret));
        ErrorExit (1);
    }

    snd_mixer_selem_id_alloca (&sid);   /* returns ptr */
    // printf("handle=%X\n", (int) handle);
    // printf("sid=%X\n", (int) sid);

    snd_mixer_selem_id_set_index (sid, 0);      /* returns void */
    // failed -- snd_mixer_selem_id_set_name(sid, "Default"); 
    // failed -- snd_mixer_selem_id_set_name(sid, "Master"); 
    // failed -- snd_mixer_selem_id_set_name(sid, "hw:0,0"); 
    // failed -- snd_mixer_selem_id_set_name(sid, "hw:0,0"); 
    // failed -- snd_mixer_selem_id_set_name(sid,  "plughw:0,0");
    // failed -- snd_mixer_selem_id_set_name(sid,  "");
    snd_mixer_selem_id_set_name (sid, VOLUME_ELEM);

    elem = snd_mixer_find_selem (handle, sid);
    //  printf("elem=%X \n", (int) elem);

    ret = snd_mixer_selem_get_playback_volume_range (elem, &min, &max);
    if (ret < 0)
    {
        printf ("snd_mixer_selem_get_playback_volume_range returned error: %s\n", snd_strerror (ret));
        ErrorExit (1);
    }

    ret = snd_mixer_selem_set_playback_volume_all (elem, (playback_volume * max) / 100);
    if (ret < 0)
    {
        printf ("snd_mixer_selem_get_playback_volume_range returned error: %s\n", snd_strerror (ret));
        ErrorExit (1);
    }

    ret = snd_mixer_selem_get_playback_volume (elem, 0, &read_volume);
    if (ret < 0)
    {
        printf ("snd_mixer_selem_get_playback_volume_all returned error: %s\n", snd_strerror (ret));
        ErrorExit (1);
    }

    //  printf("read_volume=%ld\n\n\n", read_volume);
    snd_mixer_close (handle);

}

/*******************************************************************************/
static void
ErrorExit (int error_code)
{
    RestoreKeyboard ();
    exit (error_code);
}

/*******************************************************************************/
static void
SigHandler (int sig)
{
    RestoreKeyboard ();
    printf ("\nRestore keyboard complete\n");
    ErrorExit (0);
}

/*******************************************************************************/
struct termios t_save;
static void
ConfigKeyboard (void)
{
    struct termios t;
    if (tcgetattr (STDIN_FILENO, &t) == -1)
    {
        printf ("tcgetattr returned error: %d\n", errno);
        ErrorExit (1);
    }

    /* Set terminal to cannonical mode, disable echoing, poll mode */
    t_save = t;
    t.c_lflag &= ~(ICANON | ECHO);
    t.c_cc[VMIN] = 0;
    t.c_cc[VTIME] = 0;

    if (tcsetattr (STDIN_FILENO, TCSAFLUSH, &t) == -1)
    {
        printf ("tcsetattr returned error: %d\n", errno);
        ErrorExit (1);
    }

    /* Install signal handler to restore keyboard */
    if (signal (SIGINT, SigHandler) == SIG_ERR)
    {
        printf ("signal returned error: %d\n", errno);
        ErrorExit (1);
    }
}

/*******************************************************************************/
static void
RestoreKeyboard (void)
{
    if (tcsetattr (STDIN_FILENO, TCSAFLUSH, &t_save) == -1)
    {
        printf ("tcsetattr returned error: %d\n", errno);
        ErrorExit (1);
    }
}

/*******************************************************************************/
static void
CheckKeyboardInput (void)
{
    char c;
    int bytes_read;

    bytes_read = read (STDIN_FILENO, &c, sizeof (c));
    if (bytes_read != 0)
    {
        if (c == 'v')
        {
            if (playback_volume > MIN_VOLUME)
            {
                --playback_volume;
                printf ("VOLUME is %u \n\n", playback_volume);
            }
            else
            {
                printf ("MIN VOLUME\n\n");
            }
        }
        else if (c == 'V')
        {
            if (playback_volume < MAX_VOLUME)
            {
                ++playback_volume;
                printf ("VOLUME is %u \n\n", playback_volume);
            }
            else
            {
                printf ("MAX VOLUME\n\n");
            }
        }
        SetVolumePercent (playback_volume);
    }
}
