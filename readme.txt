# Use the following options to rebuild for ARM64 (e.g. Salvator-x) with debug information and limited optimizations
make -Bj OPT=ARMv8 DEBUG=1

# Use the following options to rebuild for ARM64 (e.g. Salvator-x) with no debug and max optimizations
make -Bj OPT=ARMv8 

